<?php
function alusta_sessioon(){
	
	session_start();
	}
	
	function lopeta_sessioon(){
	$_SESSION = array();
	if (isset($_COOKIE[session_name()])) {
 	 setcookie(session_name(), '', time()-30000, '/');
	}
	session_destroy();
}

function kuva_login(){
	global $myurl;
	
	include("view/sisselogimine.html");
}

function kuva_reg_vorm(){	
	global $myurl;
	include("view/registreerimine.html");
}

function autendi(){
	global $myurl;
	$errors=array();
	$username="";
	$passwd="";
	if (isset($_POST['username']) && $_POST['username']!="") {
		$username=$_POST['username'];
	} else {
		$errors[]="Kasutajanimi puudu";
	}
	if (isset($_POST['passwd']) && $_POST['passwd']!="") {
		$passwd=$_POST['passwd'];
	} else {
		$errors[]="parool puudu";	
	}
	
	if (empty($errors)){
		
		if ($username=="kasutaja" && $passwd=="parool"){	
			$_SESSION['username']=$username;
			$_SESSION['user_id']=0;
			$_SESSION['teated'][]="Tere! Olete sisse loginud nimega ".htmlspecialchars($username)."!";
			header("Location: $myurl");
			
		} else {
			$errors[]="Vale kasutajanimi või parool";	
			include("view/sisselogimine.html");
		}	
	} else {
		include("view/sisselogimine.html");
	}
	
}

function logout(){
	global $myurl;
		lopeta_sessioon();
		header("Location: $myurl");
	}
	
	

?>